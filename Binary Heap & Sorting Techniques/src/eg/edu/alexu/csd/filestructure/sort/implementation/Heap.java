package eg.edu.alexu.csd.filestructure.sort.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.management.RuntimeErrorException;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class Heap<T extends Comparable<T>> implements IHeap<T>, Cloneable {
	private List<INode<T>> maxHeap;
	private List<T> data;
	private int heapSize;

	private class Node implements INode<T> {
		private int index;

		public Node(int index) {
			this.index = index;
		}

		@Override
		public INode<T> getLeftChild() {
			int leftChildIndex = index * 2 + 1;
			if (leftChildIndex >= heapSize) {
				return null;
			}
			return maxHeap.get(leftChildIndex);

		}

		@Override
		public INode<T> getRightChild() {
			int rightChildIndex = (index + 1) * 2;
			if (rightChildIndex >= heapSize) {
				return null;
			}
			return maxHeap.get(rightChildIndex);
		}

		@Override
		public INode<T> getParent() {
			if (index <= 0) {
				return null;
			}
			return maxHeap.get((index - 1) / 2);
		}

		@Override
		public T getValue() {
			return data.get(index);
		}

		@Override
		public void setValue(T value) {
			data.set(index, value);
		}

	}

	@Override
	public INode<T> getRoot() {
		if (heapSize <= 0) {
			return null;
		}
		return maxHeap.get(0);
	}

	@Override
	public int size() {
		return heapSize;
	}

	@Override
	public void heapify(INode<T> node) {
		if (node == null) {
			return;
		}
		INode<T> leftChild = node.getLeftChild();
		INode<T> rightChild = node.getRightChild();
		if (leftChild == null)
			return;
		if (rightChild == null) {
			if (leftChild.getValue().compareTo(node.getValue()) > 0) {
				swapValues(leftChild, node);
			}
			return;
		}
		INode<T> max;
		if (leftChild.getValue().compareTo(rightChild.getValue()) < 0) {
			max = rightChild;
		} else {
			max = leftChild;
		}
		if (max.getValue().compareTo(node.getValue()) > 0) {
			swapValues(max, node);
			heapify(max);
		}

	}

	@Override
	public T extract() {
		if (heapSize <= 0) {
			throw new RuntimeErrorException(null);
		}
		T max = getRoot().getValue();
		swapValues(getRoot(), maxHeap.get(heapSize - 1));
		--heapSize;
		heapify(getRoot());
		return max;
	}

	@Override
	public void insert(T element) {
		if (element == null) {
			throw new RuntimeErrorException(null);
		}
		if (heapSize == 0) {
			maxHeap = new ArrayList<INode<T>>();
			data = new ArrayList<T>();
			maxHeap.add(new Node(heapSize));
			data.add(element);
			heapSize++;
			return;
		}
		INode<T> current = new Node(heapSize);
		if (data.size() > heapSize) {
			maxHeap.set(heapSize, current);
			data.set(heapSize, element);
		} else {
			maxHeap.add(current);
			data.add(element);
		}
		heapSize++;
		boolean flag = true;
		while (flag) {
			INode<T> parent = current.getParent();
			if (parent == null) {
				flag = false;
			} else if (parent.getValue().compareTo(current.getValue()) < 0) {
				swapValues(current, parent);
				current = parent;
			} else {
				flag = false;
			}

		}
	}

	@Override
	public void build(Collection<T> unordered) {
		if (unordered == null) {
			return;
		}
		maxHeap = new ArrayList<INode<T>>();
		heapSize = unordered.size();
		if (unordered instanceof List<?>) {
			data = (List<T>) unordered;
			for (int i = 0; i < data.size(); i++) {
				maxHeap.add(new Node(i));
			}
		} else {
			int i = 0;
			for (Iterator<T> iterator = unordered.iterator(); iterator.hasNext(); i++) {
				T t = iterator.next();
				maxHeap.add(new Node(i));
				data.add(t);
			}
		}
		if (heapSize > 0) {
			for (int i = maxHeap.size() / 2; i >= 0; i--) {
				heapify(maxHeap.get(i));
			}
		}
	}

	public void sort() {
		while (heapSize > 1) {
			extract();
		}
		heapSize = data.size();
	}

	private void swapValues(INode<T> x, INode<T> y) {
		T temp = x.getValue();
		x.setValue(y.getValue());
		y.setValue(temp);
	}

}
