package eg.edu.alexu.csd.filestructure.sort.implementation;

import java.util.ArrayList;
import java.util.Random;

import javax.management.RuntimeErrorException;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.ISort;

public class Sort<T extends Comparable<T>> implements ISort<T> {

	@Override
	public IHeap<T> heapSort(ArrayList<T> unordered) {
		if (unordered == null) {
			throw new RuntimeErrorException(null);
		}

		IHeap<T> heap = new Heap<T>();
		heap.build(unordered);
		((Heap<T>) heap).sort();
		return heap;
	}

	@Override
	public void sortSlow(ArrayList<T> unordered) {
		if (unordered == null) {
			throw new RuntimeErrorException(null);
		}
		for (int i = 0; i < unordered.size(); i++) {
			for (int index = 0; index < unordered.size() - i - 1; index++) {
				if (unordered.get(index).compareTo(unordered.get(index + 1)) > 0) {
					swap(index, index + 1, unordered);
				}
			}
		}
		
	}

	@Override
	public void sortFast(ArrayList<T> unordered) {
		if (unordered == null) {
			throw new RuntimeErrorException(null);
		}
		Random r =  new Random();
		if (r.nextInt(2) == 0) {
			quickSort(0, unordered.size() - 1, unordered);
		}else {
			mergeSort(0, unordered.size() - 1, unordered);
		}
	}

	private void mergeSort(int first, int last, ArrayList<T> unordered) {
		if (first < last) {
			int middle = (last - first) / 2 + first;
			mergeSort(first, middle, unordered);
			mergeSort(middle + 1, last, unordered);
			merge(first, last, middle, unordered);
		}
	}

	private void merge(int first, int last, int middle, ArrayList<T> unordered) {
		ArrayList<T> temp1 = new ArrayList<>(middle - first + 1);
		ArrayList<T> temp2 = new ArrayList<>(last - middle);
		int index = first;
		while (index <= middle) {
			temp1.add(unordered.get(index++));
		}
		while (index <= last) {
			temp2.add(unordered.get(index++));
		}
		index = first;
		int i = 0, j = 0;
		while (i < temp1.size() && j < temp2.size()) {
			if (temp1.get(i).compareTo(temp2.get(j)) < 0) {
				unordered.set(index++, temp1.get(i++));
			}else {
				unordered.set(index++, temp2.get(j++));
			}
		}
		while(i < temp1.size()) {
			unordered.set(index++, temp1.get(i++));
		}
		while(j < temp2.size()) {
			unordered.set(index++, temp2.get(j++));
		}
	}

	private void quickSort(int first, int last,ArrayList<T> unordered) {
		if (first < last) {
			int pivotIndex = partation(first, last, unordered);
			quickSort(first, pivotIndex -1, unordered);
			quickSort(pivotIndex + 1, last, unordered);
		}
		
	}
	
	private int partation(int first, int last, ArrayList<T> unordered) {
		Random r = new Random();
		int pivotIndex = r.nextInt(last - first + 1) + first;
		swap(pivotIndex, last, unordered);
		T pivot = unordered.get(last);
		pivotIndex = first;
		for (int i = first; i < last; i++) {
			if (unordered.get(i).compareTo(pivot) < 0) {
				swap(pivotIndex++, i, unordered);
			}
		}
		swap(pivotIndex, last, unordered);
		return pivotIndex;
	}
	
	private void swap(int x, int y, ArrayList<T> unordered) {
		T temp = unordered.get(x);
		unordered.set(x, unordered.get(y));
		unordered.set(y, temp);
	}

}
